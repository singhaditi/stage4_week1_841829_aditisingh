	package com.cognizant.MovieCruiser.Dao;
	import java.text.ParseException;
	import java.util.ArrayList;
	import com.cognizant.MovieCruiser.Model.Movie;



public interface MovieDao
 {		

		    ArrayList<Movie> getAllMoviesAdmin();
			
		    ArrayList<Movie> getActiveMoviesCustomer() throws ParseException;

		    ArrayList<Movie> getFavoriteMoviesCustomer(String userId);

	 	    void addFavoriteMovieCustomer(String userId, int movieId);
	 
	     	    void editMovieAdmin(Movie updateMovie, int movieId);
		
		    ArrayList<Movie> getAllMoviesCustomer();

		    void removeFavoriteMovieCustomer(String userId, int movieId);
	 
		
		
		
	}

	
	
	

