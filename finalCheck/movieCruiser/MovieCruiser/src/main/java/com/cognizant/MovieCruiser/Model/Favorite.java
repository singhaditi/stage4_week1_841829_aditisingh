package com.cognizant.MovieCruiser.Model;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Favorite 
{

	
	

	private ArrayList<Integer> favoriteMovies;


	private int id;
	
	private static final Logger lggggg = LoggerFactory.getLogger(Favorite.class);
	
	public Favorite()
 {
		lggggg.debug(" Starting of constructor........");

		favoriteMovies = null;

		LOGGER.debug("Ending Constructor.....");
	}


	public ArrayList<Integer> getFavoriteMovies()
 {
		return favoriteMovies;
	}

	public void setFavoriteMovies(ArrayList<Integer> favoriteMovies)
 {
		this.favoriteMovies = favoriteMovies;
	}
	

	

	public int getId()
 {
		return id;
	}

	public void setId(int id)
 {
		this.id = id;
	}

	
	@Override
	public String toString() {
		return "Favorite [id = " + id + ", favoriteMovies = " + favoriteMovies + "]";
	}
	
}
