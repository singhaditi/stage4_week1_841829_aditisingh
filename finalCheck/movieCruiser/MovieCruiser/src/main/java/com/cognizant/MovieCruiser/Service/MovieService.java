package com.cognizant.MovieCruiser.Service;
import java.text.ParseException;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cognizant.MovieCruiser.Dao.MovieDao;
import com.cognizant.MovieCruiser.Model.Movie;

@Service
public class MovieService 
{

	

	@Autowired
	private MovieDao md1;

	private static final Logger lggggg = LoggerFactory.getLogger(MovieService.class);

	public ArrayList<Movie> getAllMoviesAdmin()
 {
		
		lggggg.debug("movieService Starts");
		return md1.getAllMoviesAdmin();
	}

	public void editMovieAdmin(Movie updateMovie, int movieId)
 {
		
		lggggg.debug("START OF FUNCTION");
		md1.editMovieAdmin(updateMovie, movieId);

	}

	public ArrayList<Movie> getAllMoviesCustomer()
 {
	
		lggggg.debug("START OF FUNCTION");
		return md1.getAllMoviesCustomer();
	}

	public ArrayList<Movie> getActiveMoviesCustomer() throws ParseException
 {
	
		lggggg.debug("START OF FUNCTION");
		return md.getActiveMoviesCustomer();
	}

	public ArrayList<Movie> getFavoriteMoviesCustomer(String userId)
 {
	
		lggggg.debug("START OF FUNCTION");
		return md1.getFavoriteMoviesCustomer(userId);
	}

	public void addFavoriteMovieCustomer(String userId, int movieId)
 {
	
		lggggg.debug("START OF FUNCTION");
		md1.addFavoriteMovieCustomer(userId, movieId);
		LOGGER.debug("END OF FUNCTION");
	}

	public void removeFavoriteMovieCustomer(String userId, int movieId)
 {
		
		lggggg.debug("STARTING OF FUNCTION");
		md1.removeFavoriteMovieCustomer(userId, movieId);
		lggggg.debug("END OF FUNCTION");
	}
	
	
}
