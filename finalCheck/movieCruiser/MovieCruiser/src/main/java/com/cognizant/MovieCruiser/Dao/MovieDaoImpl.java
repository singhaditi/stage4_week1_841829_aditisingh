package com.cognizant.MovieCruiser.Dao;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;
import com.cognizant.MovieCruiser.Model.Favorite;
import com.cognizant.MovieCruiser.Model.Movie;

@Repository
public class MovieDaoImpl  implements MovieDao{
	

	 
	private static HashMap<String, Favorite> favoriteList = new HashMap<String, Favorite>();

	private static ApplicationContext aplcontext = new ClassPathXmlApplicationContext("movie.xml");

	private static ArrayList<Movie> movieLlist = aplcontext.getBean("movieList", java.util.ArrayList.class);


	private static final Logger lggggg = LoggerFactory.getLogger(MovieDaoImpl.class);

	@Override
	public void addFavoriteMovieCustomer(String userId, int movieId) {
		lggggg.debug("STARTS");
		if (favoriteList.isEmpty()) {
			Favorite fav = new Favorite();
			ArrayList<Integer> list = new ArrayList<Integer>();
			lggggg.debug("BEFORE: " + list.toString());
			list.add(movieId);
			fav.setFavoriteMovies(list);
			favoriteList.put(userId, fav);
			lggggg.debug("AFTER: " + list.toString());
		} else if (favoriteList.containsKey(userId)) {
			Favorite fav = favoriteList.get(userId);
			ArrayList<Integer> list = fav.getFavoriteMovies();
			lggggg.debug("BEFORE: " + list.toString());
			if (!list.contains(movieId))
				list.add(movieId);

			fav.setFavoriteMovies(list);
			favoriteList.put(userId, fav);
			lggggg.debug("AFTER: " + list.toString());
		}
 else
 {
			Favorite fav = new Favorite();
			ArrayList<Integer> list = new ArrayList<Integer>();
			LOGGER.debug("BEFORE: " + list.toString());
			list.add(movieId);
			fav.setFavoriteMovies(list);
			favoriteList.put(userId, fav);
			lggggg.debug("AFTER: " + list.toString());
		}

		lggggg.debug("END");
	}



	
	@Override
	public void removeFavoriteMovieCustomer(String userId, int movieId) {
		lggggg.debug("STARTING removeFavoriteMovieCustomer");
		if (favoriteList.containsKey(userId)) {
			Favorite fav = favoriteList.get(userId);
			ArrayList<Integer> list = fav.getFavoriteMovies();
			lggggg.debug("BEFORE: " + list.toString());
			if (list.contains(movieId)) {
				int index = list.indexOf(movieId);
				list.remove(index);
			}
			fav.setFavoriteMovies(list);
			favoriteList.put(userId, fav);
			lggggg.debug("AFTER: " + list.toString());
		}
		lggggg.debug("ENDING removeFavoriteMovieCustomer");
	}

	@Override
	public ArrayList<Movie> getAllMoviesCustomer() {
		lggggg.debug("STARTS");
		return movieLlist;
	}

	@Override
	public ArrayList<Movie> getActiveMoviesCustomer() throws ParseException {

		lggggg.debug("STARTS");
		SimpleDateFormat sdfo = new SimpleDateFormat("dd/MM/yyyy");
		Date currentDate = sdfo.parse("15/06/2020");

		ArrayList<Movie> finalList = (ArrayList<Movie>) MOVIE_LIST.stream().filter(movie -> (movie.isActive() && movie.getDateOfLaunch().before(currentDate))).collect(Collectors.toList());

		lggggg.debug("END");
		return finalList;
	}

	@Override
	public ArrayList<Movie> getFavoriteMoviesCustomer(String userId) {
		lggggg.debug("STARTS");
		if (favoriteList.containsKey(userId)) {
			Favorite fav = favoriteList.get(userId);
			ArrayList<Integer> list = fav.getFavoriteMovies();
			ArrayList<Movie> movielist = new ArrayList<Movie>();

			for (int i = 0; i < list.size(); i++) {

				for (int j = 0; j < movieLlist.size(); j++) {
					if (movieLlist.get(j).getId() == list.get(i)) {
						movielist.add(movieLlist.get(j));
					}
				}

			}

			return movielist;
		}
		return null;
	}
	
		@Override
	public ArrayList<Movie> getAllMoviesAdmin() 
{
		lggggg.debug("STARTS");
		return movieLlist;
	}

	@Override
	public void editMovieAdmin(Movie updateMovie, int movieId) 
{
		
		lggggg.debug(updateMovie.toString());
	}

	}