package com.cognizant.MovieCruiser.Controller;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cognizant.MovieCruiser.MovieCruiserApplication;
import com.cognizant.MovieCruiser.Model.Movie;
import com.cognizant.MovieCruiser.Service.MovieService;

@RestController
@RequestMapping("/admin/movies")
public class MovieController {

	
	@Autowired
	private MovieService ms;
	
	private static final Logger lggg= LoggerFactory.getLogger(MovieController.class);
	
	@GetMapping
	public ArrayList<Movie> getAllMoviesAdmin() {

		lggg.info("Movie Controller Begins");

		return ms.getAllMoviesAdmin();
	}
	
	@PutMapping("/{movieId}")
	public void editMovieAdmin(@RequestBody movie updateMovie, @PathVariable("movieId") int movieId)
 {

		lggg.info("Begin");

		ms.editMovieAdmin(updateMovie, movieId);
	}
	
}
