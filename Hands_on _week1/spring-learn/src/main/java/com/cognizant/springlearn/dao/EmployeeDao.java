package com.cognizant.springlearn.dao;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.cognizant.springlearn.Employee;
import com.cognizant.springlearn.SpringLearnApplication;
import com.cognizant.springlearn.service.exception.EmployeeNotFoundException;

public class EmployeeDao 
{

	private static final Logger objjj = LoggerFactory.getLogger(SpringLearnApplication.class);

	static ApplicationContext aplcontext = new ClassPathXmlApplicationContext("employee.xml");
	static ArrayList<Employee> empLlist = aplcontext.getBean("employeeList", java.util.ArrayList.class);

	public static void updateEmployee(Employee employee) throws EmployeeNotFoundException
 {

		Employee empList = empLlist.stream().filter(emp -> emp.getId() == employee.getId()).findAny().orElseThrow(() -> new EmployeeNotFoundException());
		
		objjj.info("OLD EMPLOYEES ARE : " + empList.toString());
		
		objjj.info("Employee which are updated : " + employee.toString());
	}


	public static ArrayList<Employee> getAllEmployees() 
{
		return empLlist;
	}

		public static void deleteEmployee(Employee employee) throws EmployeeNotFoundException
 {

			Employee empList = empLlist.stream().filter(emp -> emp.getId() == employee.getId()).findAny().orElseThrow(() -> new EmployeeNotFoundException());

			objjj.info("Employee Which Are Removed : " + empList.toString());

	}
}
