package com.cognizant.springlearn.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.cognizant.springlearn.Country;
import com.cognizant.springlearn.SpringLearnApplication;
import com.cognizant.springlearn.service.CountryService;
import com.cognizant.springlearn.service.exception.CountryNotFoundException;

@RestController
public class CountryController {

	private static final Logger objjj = LoggerFactory.getLogger(SpringLearnApplication.class);
		

		
		@GetMapping("/countries")
		public ArrayList<Country> getAllCountries() 
		{

		 ApplicationContext apcontext = new ClassPathXmlApplicationContext("country.xml");

		 ArrayList<Country> a =  apcontext.getBean("countryList", java.util.ArrayList.class);

		  return a;
  			}

	 	@RequestMapping("/country")
	        public Country  getCountryIndia()
			{

		  ApplicationContext apcontext = new ClassPathXmlApplicationContext("country.xml");

		  Country c = (Country) apcontext.getBean("country", Country.class);

		  return c;
		
	}


	@GetMapping("/countries/{code}")
	public Country getCountry(@PathVariable("code") String code) throws CountryNotFoundException
		{
		CountryService countryservicess = new CountryService();
	     
 		Country cde = null;

		cde = countryservicess.getCountry(code);
	
		return cde;
		
		}
	
	@PostMapping("/countries")
	public Country addCountry(@RequestBody @Valid Country country) 
{
	
	objjj.info(" START Inside ADD Country. ");
        
        ValidatorFactory vlfactory = Validation.buildDefaultValidatorFactory();

        Validator validatorss = (Validator) vlfactory.getValidator();

        Set<ConstraintViolation<Country>> violationsss = validatorss.validate(country);
        List<String> errorsss = new ArrayList<String>();

      
        for (ConstraintViolation<Country> violationss : violationsss)
 {
           
		 errorsss.add(violationss.getMessage());
        }

        
        if (violationsss.size() > 0) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, errorsss.toString());
        }
		return country;
	}
	
	
	
	
	
}
