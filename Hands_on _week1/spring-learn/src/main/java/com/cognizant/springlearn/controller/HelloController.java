package com.cognizant.springlearn.controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cognizant.springlearn.SpringLearnApplication;
@RestController
public class HelloController
 {
	private static final Logger objjj = LoggerFactory.getLogger(SpringLearnApplication.class);

	@GetMapping("/")
	public String sayHello()
 {

		objjj.info("GET HELLOWORLD STARTS");

		return "HelloWorld!";
		
		
	}
	

}
